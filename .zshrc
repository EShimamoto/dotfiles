# 環境変数
export LANG=ja_JP.UTF-8
export PATH=$HOME/.nodebrew/current/bin:$PATH

# エイリアス
alias relogin='exec $SHELL -l'

# git設定
autoload -U colors; colors
autoload -Uz VCS_INFO_get_data_git; VCS_INFO_get_data_git 2> /dev/null
autoload -Uz vcs_info
zstyle ':vcs_info:*' enable git svn
zstyle ':vcs_info:*' formats '%{'${fg[red]}'%}[%s %b] %{'$reset_color'%}'


setopt prompt_subst
precmd () {
  LANG=en_US.UTF-8 vcs_info
  LOADAVG=$(sysctl -n vm.loadavg | perl -anpe '$_=$F[1]')
  PROMPT='${vcs_info_msg_0_}%{${fg[yellow]}%}%* ($LOADAVG) %%%{$reset_color%} '
}
RPROMPT='%{${fg[green]}%}%/%{$reset_color%}'

# PCRE 互換の正規表現を使う
setopt re_match_pcre
setopt no_beep

# 補完機能を有効にする
autoload -Uz compinit && compinit

# 補完時、大文字小文字を区別しない
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'

# プロンプトが表示されるたびにプロンプト文字列を評価、置換する
setopt prompt_subst

export PGDATA=/usr/local/var/postgres
